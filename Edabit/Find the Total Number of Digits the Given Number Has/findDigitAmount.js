function findDigitAmount(num) {
    return [...num.toString()].length;

}
/*
* Alternative Solution:
*
*   const findDigitAmount = num => String(num).length;
* */
console.log(findDigitAmount(19234));