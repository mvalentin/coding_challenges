const month_name = (num) => {
    const m = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if (num > 0 && num <= 12) {
        return m[num - 1];
    }
};

/* ALTERNATIVE ANSWERS

const month_name = (num) => {
    const m = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if (num > 0 && num <= 12) {
        return m[num - 1];
    }
};
----------------

const month_name = month =>
  new Date(`1970 ${month} 01`).toLocaleString('en', { month: 'long' });

* */